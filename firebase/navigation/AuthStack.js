import React, {useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import LoginScreen from '../screens/LoginScreen';
import OnBordingScreen from '../screens/OnBordingScreen';
import SignupScreen from '../screens/SignupScreen'


const Stack = createStackNavigator();

const AuthStack = () => {
  const [isfirstLaunch, setIsFirstLaunch] = React.useState(null);

  useEffect(() => {
    AsyncStorage.getItem('alradyLaunched').then(value => {
      if (value === null) {
        AsyncStorage.setItem('alradyLaunched', 'true');
        setIsFirstLaunch(true);
      } else {
        setIsFirstLaunch(false);
      }
    });

    GoogleSignin.configure({
      webClientId: '957055228912-9c2da5ha7ftrun476bu28uuf5mst28rc.apps.googleusercontent.com',
    });

  }, []);

  if (isfirstLaunch === null) {
    return null;
  } else if (isfirstLaunch === true) {
    
     routeName = 'Onbording'
    
  } else {
    routeName = 'Login'
  }

  return (
  
      <Stack.Navigator initialRouteName={routeName}
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Onbording" component={OnBordingScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Signup" component={SignupScreen} />
      </Stack.Navigator>
 
  );
};

export default AuthStack;
