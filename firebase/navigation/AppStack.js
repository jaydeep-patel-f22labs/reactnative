import React, {useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen'

const Stack = createStackNavigator()

const AppStack = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name='home' component={HomeScreen} />
        </Stack.Navigator>
    )
}

export default AppStack