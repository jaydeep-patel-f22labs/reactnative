import React, {useContext} from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import { AuthContext } from '../navigation/AuthProvider'

const HomeScreen = () => {

    const {user, logout} = useContext(AuthContext)

    return(
        <View style={styles.conatiner}>
            <Text>Home Screen</Text>
            <Button title='Logout' onPress={() => logout()}></Button>
        </View>
    )
}

export default HomeScreen

const styles = StyleSheet.create({
    conatiner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})