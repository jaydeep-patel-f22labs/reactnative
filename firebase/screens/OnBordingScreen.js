import React from 'react';
import {View, Text, Button, StyleSheet, Image} from 'react-native';
import Onboarding from 'react-native-onboarding-swiper';

const OnBordingScreen = ({navigation}) => {
  return (
    <Onboarding

    onSkip={() => navigation.replace('Login')}
    onDone={() => navigation.navigate('Login')}
      pages={[
        {
          backgroundColor: 'pink',
          image: <Image source={require('../assets/onboarding-img1.png')} />,
          title: 'Onboarding 1',
          subtitle: 'Done with React Native Onboarding Swiper',
        },
        {
            backgroundColor: 'gray',
            image: <Image source={require('../assets/onboarding-img2.png')} />,
            title: 'Onboarding 2',
            subtitle: 'Done with React Native Onboarding Swiper',
        },
        {
            backgroundColor: 'pink',
            image: <Image source={require('../assets/onboarding-img3.png')} />,
            title: 'Onboarding 3',
            subtitle: 'Done with React Native Onboarding Swiper',
        },
      ]}
    />
  );
};

export default OnBordingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
