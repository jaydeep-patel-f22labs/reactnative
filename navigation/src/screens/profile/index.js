import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Header from '../../components/header';
import commonStyles from '../../../commonStyles';

class Profle extends React.Component {
  
  render() {
    return (
      <>
        <SafeAreaView>
          <Header navigation={this.props.navigation} Title={'My Profile'} isAtRoot={true} />
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
          >
            
            <View style={[commonStyles.column, commonStyles.header]}>
              <Image style={commonStyles.logo} source={require('../../../assets/icon.png')} />
            </View>
            
            

            <View style={styles.fieldGroup}>
              <Text style={styles.label}>Name</Text>
              <Text>Jaydeep Patel</Text>
            </View>
            <View style={styles.fieldGroup}>
              <Text style={styles.label}>Gender</Text>
              <Text>Male</Text>
            </View>
            <View style={styles.fieldGroup}>
              <Text style={styles.label}>Address</Text>
              <Text>India</Text>
            </View>
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  fieldGroup: {
    marginTop: 50,
    marginBottom: 10,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  label: {
    fontWeight: 'bold'
  },
  
});

export default (props) => {
  const navigation = useNavigation();
  return (
    <Profle {...props} navigation={navigation} />
  )
}