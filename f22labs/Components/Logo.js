import React from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {
  useTheme,
  DefaultTheme,
  Provider as PaperProvider,
} from 'react-native-paper';
import Ionicons from 'react-native-vector-icons/Ionicons';

const Logo = ({lable}) => {
  const {colors, fontFamilies} = useTheme();
  return (
    <View style={[styles.container, {backgroundColor: colors.black}]}>
      <View style={styles.logo}>
        <Ionicons name="albums" size={38} color="white" />
      </View>
      <Text
        style={[
          styles.textColor,
          {color: colors.white, fontFamily: fontFamilies.sans},
        ]}>
        {lable}
      </Text>
    </View>
  );
};

const styles =  StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
   
  },
  textColor: {
    color: 'white',
    textTransform: 'uppercase',
    fontSize: 40,
  },
  logo: {
    marginRight: '5%',
    marginTop: '1%',
  },
});

export default Logo;
