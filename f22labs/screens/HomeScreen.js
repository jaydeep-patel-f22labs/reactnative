import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {
  useTheme,
  DefaultTheme,
  Provider as PaperProvider,
} from 'react-native-paper';
import Logo from '../Components/Logo.js';

const HomeScreen = props => {
  const {colors, fontFamilies} = useTheme();
  return (
    <View style={[styles.container, {backgroundColor: colors.black}]}>
      <View style={{marginTop: 165}}>
        <Logo lable="satoshi" />
      </View>

      <View style={styles.container2}>
        <Text
          style={[
            styles.textColor,
            {color: colors.white, fontFamily: fontFamilies.sans},
          ]}>
          Home Screen
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  container2:{
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  textColor: {
    color: 'white',
    textTransform: 'uppercase',
    fontSize: 40,
    marginTop: 94

  },
});

export default HomeScreen;
