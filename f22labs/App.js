import React, {useState, useEffect} from 'react';
import {Platform, StyleSheet, Text, View, StatusBar} from 'react-native';
import HomeScreen from './screens/HomeScreen';
import SplashScreen from './screens/SplashScreen';
import {
  useTheme,
  DefaultTheme,
  Provider as PaperProvider,
  configureFonts,
} from 'react-native-paper';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'tomato',
    accent: 'yellow',
    white: 'white',
    black: 'black',
  },
  fontFamilies: {
    ...DefaultTheme.fontFamilies,
    sans: 'futura-light-bt',
  },
};

const App = props => {
  const {colors, fontFamilies} = useTheme();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setLoading(false), 3000);
  }, []);

  return (
    <PaperProvider theme={theme}>
      <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "transparent" translucent = {true}/>
      {loading === false ? <HomeScreen /> : <SplashScreen />}
    </PaperProvider>
  );
};

const styles = StyleSheet.create({});

export default App;
