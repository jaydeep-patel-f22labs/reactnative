import React from 'react';
import {View, Text, Button, StyleSheet, StatusBar} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {AuthContext} from '../components/context';

const HomeScreen = ({navigation}) => {
  const {signOut, toggleTheme} = React.useContext(AuthContext);

  const {colors} = useTheme();

  const theme = useTheme();

  return (
    <View style={styles.container}>
      <Button
        title="signout"
        onPress={() => {
          signOut();
        }}
      />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
  },
});
